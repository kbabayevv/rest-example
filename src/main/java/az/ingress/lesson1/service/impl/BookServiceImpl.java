package az.ingress.lesson1.service.impl;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.repository.BookRepository;
import az.ingress.lesson1.service.BookService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public BookResponseDto createOrUpdateBook(UUID id, BookRequestDto dto) {
        Book book = bookRepository.findById(id).map(b -> {
            b.setAuthor(dto.getAuthor());
            b.setName(dto.getName());
            b.setPageCount(dto.getPageCount());
            return b;
        }).orElseGet(() -> {
                    Book newBook = new Book(dto);
                    newBook.setId(id);
                    return newBook;
                }
        );
        return new BookResponseDto(bookRepository.save(book));
    }

    @Override
    public BookResponseDto getBook(UUID id) {
        Book book =bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book not found with id " + id));
        return new BookResponseDto(book);
    }

    @Override
    public List<BookResponseDto> getAllBooks() {
        return bookRepository.findAll().stream().map(BookResponseDto::new).toList();
    }

    @Override
    public void deleteBook(UUID id) {
        bookRepository.findById(id).ifPresent(bookRepository::delete);
    }
}
