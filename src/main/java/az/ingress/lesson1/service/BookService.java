package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;

import java.util.List;
import java.util.UUID;

public interface BookService {

    BookResponseDto createOrUpdateBook(UUID id,BookRequestDto dto);

    void deleteBook(UUID id);

    BookResponseDto getBook(UUID id);

    List<BookResponseDto> getAllBooks();
}
