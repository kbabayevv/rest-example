package az.ingress.lesson1.model;

import az.ingress.lesson1.dto.BookRequestDto;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Book {

    @Id
    UUID id;

    String author;
    String name;
    int pageCount;

    public Book(BookRequestDto dto) {
        this.author = dto.getAuthor();
        this.name = dto.getName();
        this.pageCount = dto.getPageCount();
    }
}
