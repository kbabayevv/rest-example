package az.ingress.lesson1.dto;

import az.ingress.lesson1.model.Book;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookResponseDto {
    UUID id;
    String author;
    String name;
    int pageCount;

    public BookResponseDto(Book book) {
        this.id = book.getId();
        this.author = book.getAuthor();
        this.name = book.getName();
        this.pageCount = book.getPageCount();
    }
}
