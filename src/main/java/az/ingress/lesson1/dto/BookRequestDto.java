package az.ingress.lesson1.dto;

import az.ingress.lesson1.model.Book;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookRequestDto {
    String author;
    String name;
    int pageCount;

}
